#!/bin/bash

# Update CentOS package index
yum update -y

# Install necessary packages for Docker
yum install -y yum-utils device-mapper-persistent-data lvm2

# Add Docker repository to YUM
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Install Docker CE
yum install -y docker-ce docker-ce-cli containerd.io

# Start and enable Docker service
systemctl start docker
systemctl enable docker

# Verify Docker installation by running the hello-world container
docker run hello-world

